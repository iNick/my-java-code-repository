# my-java-code-repository

## 借用工具
WxJava: https://gitee.com/binary/weixin-java-tools
WxJava文档: 
    - http://www.binary.ac.cn/weixin-java-pay-javadoc/index.html?overview-summary.html
    - http://www.binary.ac.cn/weixin-java-miniapp-javadoc/index.html?overview-summary.html

#### 介绍
我的Java代码库


#### 用Emoji表情提交代码
https://github.com/muwenzi/Program-Blog/issues/71
---

#### 开发
⭐️ new feature(xxxfile): add some files 增加新功能比如新增某个模块 :star:

🎨 UI update(xxxfile): 界面样式相关更新 :art:

🔥 API change(xxxfile): change some API 重大更新提示，比如修改字段等 :fire:

👕 refactor(xxxfile): 包括文件代码结构以及代码风格的重构 :shirt:

(optional) 🔨 refactor(xxxfile): 包括文件代码结构以及代码风格的重构 :hammer:

🚀 improvement(xxxfile): 重大改进，提升性能如修改某个方法或算法 :rocket:

---

#### Bug
🐛 bugfix(xxxfile): fix some bug :bug:

(optional) 🐞 bugfix(xxxfile): fix some bug :beetle:

🚑 hotfix(xxxfile): fix some online bug :ambulance:

---

#### 其他
🔧 config(xxxfile): 修改配置文件 :wrench:

📝 docs(xxxfile): 添加文档说明 :memo:

✅ test(xxxfile): 添加测试用例 :white_check_mark:

🔒 security(xxxfile): 提高代码的安全性 :lock:

---

#### 版本
🎉 release(xxx): v4.7.0 :tada:

📦 build(xxx): for v4.7.0 :package:

➕ dependency(xxx): add eslint :heavy_plus_sign:

➖ dependency(xxx): remove dependency :heavy_minus_sign:

⬆️ dependency(xxx): upgrade react to 15.1.0 升级依赖库:arrow_up:

⬇️ dependency(xxx): degrade react to 15.1.0 降级依赖库:arrow_down: