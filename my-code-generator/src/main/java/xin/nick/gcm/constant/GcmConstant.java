package xin.nick.gcm.constant;

/**
 * 常量
 * @author Nick
 * @date 2022/2/27
 */
public interface GcmConstant {


    String PRI_KEY = "PRI";

    String[] COLUMN_TYPE_TIME = { "datetime", "time", "date", "timestamp" };

    String[] COLUMN_TYPE_NUMBER = { "tinyint", "smallint", "mediumint", "int", "number", "integer",
            "bit", "bigint", "float", "double", "decimal" };

    String[] COLUMN_TYPE_TEXT = { "tinytext", "text", "mediumtext", "longtext" };

    String[] COLUMN_TYPE_STR = { "char", "varchar", "nvarchar", "varchar2" };

    String JAVA_TYPE_STRING = "String";

    String JAVA_TYPE_INTEGER = "Integer";

    String JAVA_TYPE_LONG = "Long";

    String JAVA_TYPE_BIG_DECIMAL = "BigDecimal";

    String JAVA_TYPE_BOOLEAN = "Boolean";

    String JAVA_TYPE_LOCAL_DATA_TIME = "LocalDateTime";

    String PRI_KEY_REPLACE = "pri_key_column";

    String GENERAL_REPLACE = "general_column";

    String PRI_KEY_COLUMN_ANNOTATION = "@TableId(value = \""+ PRI_KEY_REPLACE +"\")";

    String GENERAL_COLUMN_ANNOTATION = "@TableField(value = \""+ GENERAL_REPLACE +"\")";


}
