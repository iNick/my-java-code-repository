package xin.nick.gcm;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import xin.nick.gcm.entity.ColumnInfo;
import xin.nick.gcm.entity.TableInfo;
import xin.nick.gcm.mapper.MyTableMapper;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Nick
 * @date 2022/2/21
 */
@Slf4j
@SpringBootApplication
@MapperScan("xin.nick.gcm.mapper")
public class MyCodeGenerator {


    /**
     * 1.去数据库取出信息
     *   1.1 表名,表注释
     *   1.2 表的列名,列类型,列注释
     *   1.3 将列的类型和Java的类型关联起来,用于生成entity
     * 2.找到对应的模板名,进行渲染
     */


    /**
     * 启动咯
     * @param args
     */
    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(MyCodeGenerator.class, args);
        log.info("启动了");
        log.info("查询一下数据库信息");
        MyTableMapper myTableMapper = applicationContext.getBean(MyTableMapper.class);
        List<TableInfo> tableInfoList = myTableMapper.getTableInfoList("foster_pig","pig_order");

        log.info("查询结果: {} ", tableInfoList);


        for (TableInfo tableInfo : tableInfoList) {
            String tableName = tableInfo.getTableName();
            String tableSchema = tableInfo.getTableSchema();
            List<ColumnInfo> columnInfoList = myTableMapper.getColumnInfoList(tableSchema, tableName);
            tableInfo.setColumnInfoList(columnInfoList);
        }

        log.info("添加column后的结果: {} ", tableInfoList);

        //

    }

    public static void handleDataTypeToJavaType() {

    }

}
