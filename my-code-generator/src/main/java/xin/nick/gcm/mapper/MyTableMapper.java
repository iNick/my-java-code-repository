package xin.nick.gcm.mapper;

import org.apache.ibatis.annotations.Param;
import xin.nick.gcm.entity.ColumnInfo;
import xin.nick.gcm.entity.TableInfo;

import java.util.List;

/**
 * @author Nick
 * @date 2022/2/25
 */
public interface MyTableMapper {

    /**
     * 根据表名列表获取表信息
     * @param tableNameList 表名列表
     * @return
     */
    List<TableInfo> selectDbTableListByNames(List<String> tableNameList);

    /**
     * 根据表名获取表信息
     * @param tableName
     * @return
     */
    List<TableInfo> selectTableByName(String tableName);

    /**
     * 获取数据表信息
     * @param tableSchema
     * @param tableName
     * @return
     */
    List<TableInfo> getTableInfoList(@Param("tableSchema") String tableSchema,
                                     @Param("tableName") String tableName);

    /**
     * 获取列信息列表
     * @param tableSchema
     * @param tableName
     * @return
     */
    List<ColumnInfo> getColumnInfoList(@Param("tableSchema") String tableSchema,
                                       @Param("tableName") String tableName);

}
