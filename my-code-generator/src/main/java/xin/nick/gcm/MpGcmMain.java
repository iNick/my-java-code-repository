package xin.nick.gcm;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * 代码生成器
 * @see <a href="https://baomidou.com/pages/56bac0/">MyBatis-Plus代码生成器</a>
 * @author Nick
 * @since 2022/1/24
 */
public class MpGcmMain {

    private static String url = "jdbc:mysql://127.0.0.1/my_code?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&useSSL=false&serverTimezone=GMT%2B8";
    private static String name = "root";
    private static String password = "123456";

    private static String projectPath = "E:\\mmm\\my-java-code-repository\\cms\\src\\main\\java\\";
    private static String projectMapperXmlPath = "E:\\mmm\\my-java-code-repository\\cms\\src\\main\\resources\\mapper";


    public static void main(String[] args) {
        FastAutoGenerator.create(url, name, password)
            .globalConfig(builder -> {
                builder.author("Nick")// 设置作者
                        .enableSwagger() // 开启 swagger 模式
                        .outputDir(projectPath); // 指定输出目录
            })
            .packageConfig(builder -> builder.parent("xin.nick").moduleName("")
                    .pathInfo(Collections.singletonMap(OutputFile.xml, projectMapperXmlPath)))
            .strategyConfig(builder -> {
                // 设置需要生成的表名
                builder.addInclude("t_user_role", "t_role_authority")
                        // 前缀
                        .addTablePrefix("t_");
            })
            .strategyConfig(builder -> {
                // 设置需要生成的Service
//                builder.serviceBuilder().formatServiceFileName("%sService");
            })
            .strategyConfig(builder -> {
                // 设置需要生成的Entity
                builder.entityBuilder()
                        .enableLombok()
                        .disableSerialVersionUID()
                        .logicDeleteColumnName("deleted")
                        .logicDeletePropertyName("deleted")
//                        .addSuperEntityColumns("id", "created_by", "created_time", "updated_by", "updated_time")
                        .enableTableFieldAnnotation().build();
            })

            // 使用Freemarker引擎模板，默认的是Velocity引擎模板
            .templateEngine(new FreemarkerTemplateEngine())
            .execute();

    }

}
