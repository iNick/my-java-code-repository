package xin.nick.gcm.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 数据表信息
 * @author Nick
 * @date 2022/2/22
 */
@Data
public class TableInfo {

    /**
     * 表名
     */
    @TableField("TABLE_NAME")
    public String tableName;

    /**
     * 所属数据库
     */
    @TableField("TABLE_SCHEMA")
    public String tableSchema;

    /**
     * 表注释
     */
    @TableField("TABLE_SCHEMA")
    public String tableComment;

    /**
     * 表创建时间
     */
    @TableField("TABLE_SCHEMA")
    public LocalDateTime createTime;

    /**
     * 表更新时间
     */
    @TableField("UPDATE_TIME")
    public LocalDateTime updateTime;

    /**
     * 数据列列表
     */
    @TableField(exist = false)
    public List<ColumnInfo> columnInfoList;

}
