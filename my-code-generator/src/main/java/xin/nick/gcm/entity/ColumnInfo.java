package xin.nick.gcm.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

/**
 * 列信息
 * @author Nick
 * @date 2022/2/25
 */
@Data
public class ColumnInfo {

    /**
     * 所属库名
     */
    @TableField("TABLE_SCHEMA")
    public String tableSchema;

    /**
     * 所属表名
     */
    @TableField("TABLE_NAME")
    public String tableName;

    /**
     * 列名
     */
    @TableField("COLUMN_NAME")
    public String columnName;

    /**
     * 列的键类型
     */
    @TableField("COLUMN_KEY")
    public String columnKey;

    /**
     * 数据类型
     */
    @TableField("DATA_TYPE")
    public String dataType;

    /**
     * java对应的数据类型
     */
    @TableField(exist = false)
    private String javaType;

    /**
     * 列名说明
     */
    @TableField("COLUMN_COMMENT")
    public String columnComment;

    /**
     * 列名类型
     */
    @TableField("COLUMN_TYPE")
    public String columnType;
}
