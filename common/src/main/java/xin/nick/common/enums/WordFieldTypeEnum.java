package xin.nick.common.enums;

import lombok.Getter;

/**
 * @author Nick
 * @since 2022/7/22/022
 */
@Getter
public enum WordFieldTypeEnum {

    /**
     * 文字类型
     */
    TEXT("0", "文字"),
    /**
     * 图片类型
     */
    IMAGE("1", "图片"),
    /**
     * 表格列表类型
     */
    LIST("2", "表格列表"),
    ;

    private final String code;
    private final String info;

    WordFieldTypeEnum(String code, String info) {
        this.code = code;
        this.info = info;
    }
}
