package xin.nick.common.config;

import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import xin.nick.common.interceptor.MdcInterceptor;

import javax.annotation.Resource;
import java.util.List;

/**
 * MVC配置类
 * @author Nick
 * @since 2022/1/27
 */
@Configuration
@ServletComponentScan
public class MyMvcConfig implements WebMvcConfigurer {

    @Resource
    private MdcInterceptor mdcInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(mdcInterceptor).addPathPatterns("/**");
    }

    /**
     * request包装类配置过滤器
     * @return
     */
//    @Bean
//    public FilterRegistrationBean<HttpServletRequestWrapperFilter> httpServletRequestReplacedRegistration() {
//        FilterRegistrationBean<HttpServletRequestWrapperFilter> registration = new FilterRegistrationBean<>(new HttpServletRequestWrapperFilter());
//        registration.addUrlPatterns("/*");
//        registration.addInitParameter("paramName", "paramValue");
//        registration.setName("HttpServletRequestWrapperFilter");
//        registration.setOrder(1);
//        return registration;
//    }

//    @Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//        converters.add(0, new MappingJackson2HttpMessageConverter());
//    }
}
