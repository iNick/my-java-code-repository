package xin.nick.common.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.stereotype.Component;
import xin.nick.common.filter.CustomAuthenticationFilter;

/**
 * 曾经想配置json格式获取用户登录信息,结果没弄出来
 * @author Nick
 * @since 2022/7/26/026
 */

//@Component
@Slf4j
public class HttpConfiguration extends AbstractHttpConfigurer<HttpConfiguration, HttpSecurity> {

    @Override
    public void configure(HttpSecurity builder) throws Exception {
        log.info("干了啥");
        AuthenticationManager authenticationManager =
                builder.getSharedObject(AuthenticationManager.class);
        builder.addFilter(new CustomAuthenticationFilter(authenticationManager));

    }
}

