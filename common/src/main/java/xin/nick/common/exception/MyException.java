package xin.nick.common.exception;

import lombok.Data;
import lombok.ToString;
import org.springframework.http.HttpStatus;
import xin.nick.common.entity.ResultCode;

/**
 * 服务器异常
 * @author Nick
 * @since 2022/7/27/027
 */
@Data
@ToString(callSuper = true)
public class MyException extends RuntimeException {


    private Integer code;

    private String message;

    public MyException() {
        this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
        this.message = HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
    }

    public MyException(String msg, HttpStatus httpStatus) {
        this.code = httpStatus.value();
        this.message = msg;
    }


    public MyException(String message) {
        this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
        this.message = message;
    }

    public MyException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public MyException(ResultCode resultCode) {
        this.code = resultCode.getValue();
        this.message = resultCode.getDescription();
    }

    public MyException(ResultCode resultCode, String msg) {
        this.code = resultCode.getValue();
        this.message = msg;
    }


}
