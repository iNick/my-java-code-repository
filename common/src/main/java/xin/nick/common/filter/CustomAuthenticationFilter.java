package xin.nick.common.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * 修改login请求的方式,使用json接收用户密码信息,,没成功
 * @author Nick
 * @since 2022/7/22/022
 */
@Slf4j
public class CustomAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    public static final String APPLICATION_JSON_UTF8_VALUE = "application/json;charset=UTF-8";

    private boolean postOnly = true;

    private AuthenticationManager authenticationManager;
    public CustomAuthenticationFilter (AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        super.setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if (this.postOnly && !HttpMethod.POST.matches(request.getMethod())) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        } else {

            // 如果 是JSON格式的数据,则尝试从json当中获取
            String contentType = request.getContentType();
            if (APPLICATION_JSON_UTF8_VALUE.equals(contentType)
                    || MediaType.APPLICATION_JSON_VALUE.equals(contentType)) {

                String username = "";
                String password = "";

                try (InputStream is = request.getInputStream()) {
                    ObjectMapper mapper = new ObjectMapper();
                    Map<String,String> authenticationBean = mapper.readValue(is, Map.class);
                    username = authenticationBean.get("username");
                    password = authenticationBean.get("password");
                } catch (IOException e) {
                    log.error("登录获取数据异常了",e);
                }
                username = username.trim();
                UsernamePasswordAuthenticationToken authRequest = UsernamePasswordAuthenticationToken.unauthenticated(username, password);
                this.setDetails(request, authRequest);
                return this.authenticationManager.authenticate(authRequest);
            }
            return super.attemptAuthentication(request, response);
        }

    }


}
