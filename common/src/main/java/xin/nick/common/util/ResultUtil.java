package xin.nick.common.util;

import com.alibaba.fastjson.JSON;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.codec.Utf8;
import org.springframework.util.StringUtils;
import xin.nick.common.constant.SystemConstants;
import xin.nick.common.entity.Result;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Nick
 * @since 2022/7/21/021
 */
public class ResultUtil {

    public static void responseResult(HttpServletResponse response, Result result) throws IOException {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();
        writer.println(JSON.toJSONString(result));
        writer.flush();
    }

}
