package xin.nick.common.util;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import xin.nick.common.constant.SystemConstants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Nick
 * @date 2022/8/2
 */
public class ServletUtil {


    private static final String TOKEN_REGEX_STRING = "Bearer\\s(.+)";
    private static final Pattern TOKEN_PATTERN = Pattern.compile(TOKEN_REGEX_STRING);

    public static String getToken() {

        return getToken(getRequest());
    }

    public static String getToken(HttpServletRequest request) {

        String token = request.getHeader(SystemConstants.TOKEN_KEY);

        String authorizationToken = request.getHeader(SystemConstants.HEADER_AUTHORIZATION);
        if (StringUtils.hasLength(authorizationToken)) {
            Matcher m = TOKEN_PATTERN.matcher(authorizationToken);
            if (m.find()) {
                token = m.group(1);
            }
        }

        if (!StringUtils.hasLength(token)) {
            token = request.getHeader(SystemConstants.TOKEN_KEY);
            if (!StringUtils.hasLength(token)) {
                token = request.getParameter(SystemConstants.TOKEN_KEY);
            }
        }

        return token;
    }


    public static HttpServletRequest getRequest() {
        ServletRequestAttributes servletRequestAttributes = getRequestAttributes();
        HttpServletRequest request = servletRequestAttributes.getRequest();
        return request;
    }

    public static HttpServletResponse getResponse() {
        ServletRequestAttributes servletRequestAttributes = getRequestAttributes();
        HttpServletResponse response = servletRequestAttributes.getResponse();
        return response;
    }

    /**
     * 获取session
     */
    public static HttpSession getSession() {
        return getRequest().getSession();
    }

    public static ServletRequestAttributes getRequestAttributes() {
        try {
            RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
            return (ServletRequestAttributes) attributes;
        } catch (Exception e) {
            return null;
        }
    }

    public static Page getQueryPage() {
        HttpServletRequest request = getRequest();
        String sizeString = request.getParameter("size");
        String currentString = request.getParameter("current");
        Long current = 1L;
        Long size = 10L;

        try {
            size = Long.valueOf(sizeString);
        } catch (Exception e) {
        }
        try {
            current = Long.valueOf(currentString);
        } catch (Exception e) {
        }

        return new Page(current, size);
    }
}
