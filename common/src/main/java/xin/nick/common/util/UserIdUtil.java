package xin.nick.common.util;

import xin.nick.common.entity.ResultCode;

/**
 * 用户id
 * @author Nick
 * @date 2021/11/9
 */
public class UserIdUtil {

    private static final ThreadLocal<Long> USER_ID_THREAD_LOCAL = new ThreadLocal<>();

    public static void setUserId(Long userId) {
        USER_ID_THREAD_LOCAL.set(userId);
    }

    public static Long getUserId() {
        Long userId = USER_ID_THREAD_LOCAL.get();
        MyAssert.notNull(userId, ResultCode.UNAUTHORIZED);
        return userId;
    }

    public static Long getUserIdNotCheck() {
        return USER_ID_THREAD_LOCAL.get();
    }

    public static void removeUserId() {
        USER_ID_THREAD_LOCAL.remove();
    }
}
