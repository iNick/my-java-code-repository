package xin.nick.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

/**
 * @author Nick
 * @since 2022/7/21/021
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserGrantedAuthority implements GrantedAuthority {

    /**
     * 授权的权限
     */
    private String authority;

}
