package xin.nick.common.entity;

import lombok.Data;
import xin.nick.common.enums.WordFieldTypeEnum;

import java.util.List;

/**
 * word处理字段对象
 * @author Nick
 * @since 2022/7/22/022
 */
@Data
public class WordField {

    /**
     * 字段类型
     */
    private WordFieldTypeEnum fieldType;

    /**
     * 字段内容
     */
    private String content;

    /**
     * 图片地址
     */
    private String imageUrl;

    /**
     * 图片宽度
     */
    private int imageWidth;

    /**
     * 图片高度
     */
    private int imageHeight;


    /**
     * 表格行列表
     */
    private List<List<String>> tableRowList;

}
