package xin.nick.common.entity;

/**
 * 统一返回对象代码
 * @author Nick
 * @date 2021/11/29
 */
public enum ResultCode {

    /**
     * 14--- 客户端错误
     * 15--- 服务器业务错误
     * 这里准备每个错误一个单独错误码的
     * 然而只是想想,没有用上
     */

    OK(200, "OK"),
    SERVER_ERROR(500, "服务器异常"),
    UNAUTHORIZED(401, "用户未登录"),
    FORBIDDEN(403, "用户无权限"),
    NOT_FOUND(404, "页面不存在"),
    PARAM_ERROR(400, "参数异常"),
    FILE_EMPTY(14002, "文件为空"),
    VERIFY_CODE_EXPIRE(14003, "验证码过期"),
    VERIFY_CODE_ERROR(14004, "验证码错误"),
    ACCOUNT_OR_PASSWORD_ERROR(14005, "账号或着密码错误"),
    PASSWORD_FORMAT_ERROR(14006, "密码格式错误"),
    REQUEST_METHOD_ERROR(14007, "请求方式不支持"),
    PARAM_FORMAT_ERROR(14008, "请求参数格式错误"),
    PARAM_EMPTY_ERROR(14009, "参数类型缺失"),
    PARAM_NOT_SUPPORTED(14010, "参数类型不支持"),
    DATA_EMPTY(14011, "数据异常"),
    DATA_ERROR(15001, "数据不存在"),
    UPLOAD_FILE_ERROR(15002, "文件上传失败"),
    STOCK_LESS(15003, "库存不足"),
    SERVICE_NOT_ALLOWED(15004, "业务不允许"),
    DATA_EXPORT_ERROR(15005, "数据导出异常"),
    ;

    private Integer value;
    private String description;

    ResultCode() {

    }

    ResultCode(Integer value, String description) {
        this.value = value;
        this.description = description;
    }

    public Integer value() {
        return value;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
