package xin.nick.common.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.slf4j.MDC;
import xin.nick.common.constant.SystemConstants;

import java.io.Serializable;

/**
 * @author Nick
 * @date 2021/11/5
 */
@Schema(description = "统一返回对象")
@Data
public class Result<T> implements Serializable {

    @Schema(description = "状态代码")
    private int code;

    @Schema(description = "返回消息")
    private String msg;

    @Schema(description = "返回数据")
    private T data;

    @Schema(description = "请求id")
    private String requestId;

    /** 状态码 */
    public static final String CODE_TAG = "code";

    /** 返回内容 */
    public static final String MSG_TAG = "msg";

    /** 数据对象 */
    public static final String DATA_TAG = "data";

    /**
     * 初始化一个新创建的 Result 对象，使其表示一个空消息。
     */
    public Result() {
    }

    public Result(T data) {
        this.code = ResultCode.OK.value();
        this.msg = ResultCode.OK.getDescription();
        this.data = data;
        this.requestId = MDC.get(SystemConstants.REQUEST_ID);
    }

    public Result(String msg) {
        this.code = ResultCode.OK.value();
        this.msg = msg;
        this.requestId = MDC.get(SystemConstants.REQUEST_ID);
    }

    public Result(int code, String msg) {
        this.code = code;
        this.msg = msg;
        this.requestId = MDC.get(SystemConstants.REQUEST_ID);
    }

    public Result(ResultCode resultCode) {
        this.code = resultCode.getValue();
        this.msg = resultCode.getDescription();
        this.requestId = MDC.get(SystemConstants.REQUEST_ID);
    }

    public Result(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.requestId = MDC.get(SystemConstants.REQUEST_ID);
    }


    /**
     * 返回成功消息
     *
     * @param msg 返回内容
     * @param data 数据对象
     * @return 成功消息
     */
    public static <T> Result<T> success(String msg, T data)
    {
        return new Result<>(ResultCode.OK.value(), msg, data);
    }

    /**
     * 返回成功数据
     *
     * @return 成功消息
     */
    public static <T> Result<T> success(T data)
    {
        return Result.success(ResultCode.OK.getDescription(), data);
    }

    /**
     * 返回成功消息
     *
     * @param msg 返回内容
     * @return 成功消息
     */
    public static <T> Result<T> successMsg(String msg)
    {
        return Result.success(msg, null);
    }

    /**
     * 返回成功消息
     *
     * @return 成功消息
     */
    public static <T> Result<T> success()
    {
        return Result.successMsg(ResultCode.OK.getDescription());
    }


    /**
     * 返回错误消息
     *
     * @param msg 返回内容
     * @param data 数据对象
     * @return 警告消息
     */
    public static <T> Result<T> error(String msg, T data)
    {
        return new Result<>(ResultCode.SERVER_ERROR.value(), msg, data);
    }

    /**
     * 返回错误消息
     *
     * @param code 状态码
     * @param msg 返回内容
     * @return 警告消息
     */
    public static <T> Result<T> error(int code, String msg)
    {
        return new Result<>(code, msg, null);
    }

    /**
     * 返回错误消息
     *
     * @param msg 返回内容
     * @return 警告消息
     */
    public static <T> Result<T> error(String msg)
    {
        return Result.error(msg, null);
    }

    /**
     * 返回错误消息
     *
     * @return 错误消息
     */
    public static <T> Result<T> error()
    {
        return Result.error(ResultCode.SERVER_ERROR);
    }

    /**
     * 返回错误消息
     *
     * @return 错误消息
     */
    public static <T> Result<T> error(ResultCode resultCode)
    {
        return Result.error(resultCode.getValue(), resultCode.getDescription());
    }

    /**
     * 返回错误消息
     *
     * @return 错误消息
     */
    public static <T> Result<T> error(ResultCode resultCode, String msg)
    {
        return Result.error(resultCode.getValue(), msg);
    }

    /**
     * 自定义返回消息
     * @param code
     * @param msg
     * @param <T>
     * @return 自定义返回消息
     */
    public static <T> Result<T> custom(Integer code, String msg) {
        return new Result<>(code, msg);
    }

    public static <T> Result<T> custom(ResultCode resultCode) {
        return new Result<>(resultCode);
    }

    public static <T> Result<T> custom(ResultCode resultCode, String msg) {
        return new Result<>(resultCode.getValue(), msg);
    }


    /**
     * 参数错误
     * @param msg
     * @param <T>
     * @return
     */
    public static <T> Result<T> paramError(String msg) {
        return new Result<>(ResultCode.PARAM_ERROR.getValue(), msg);
    }

    /**
     * 数据错误
     * @param msg
     * @param <T>
     * @return
     */
    public static <T> Result<T> dataError(String msg) {
        return new Result<>(ResultCode.DATA_ERROR.getValue(), msg);
    }

}



