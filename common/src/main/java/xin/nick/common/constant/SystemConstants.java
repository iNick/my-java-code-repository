package xin.nick.common.constant;

/**
 * @author Nick
 * @since 2022/1/27
 */
public interface SystemConstants {

    /**
     * 请求id
     */
    String REQUEST_ID = "requestId";

    String UTF8 = "UTF-8";

    /**
     * header Authorization
     */
     String HEADER_AUTHORIZATION = "Authorization";

    /**
     * token获取的key
     */
    String TOKEN_KEY = "token";


    /**
     * servlet 异常字段
     */
    String SERVLET_ERROR_EXCEPTION = "javax.servlet.error.exception";


    /**
     * 系统错误地址
     */
    String ERROR_PATH = "/error";


    /**
     * 登录地址
     */
    String LOGIN_PATH = "/api/login";


    /**
     * API地址
     */
    String API_PATH = "/api";

    /**
     * 退出登录地址
     */
    String LOGOUT_PATH = "/api/logout";

    /**
     * 退出登录地址
     */
    String CODE_PATH = "/api/code";

    /**
     * 退出登录地址
     */
    String REFRESH_TOKEN_PATH = "/api/refresh/token";


    /**
     * 用户token缓存key
     */
    String REDIS_APP_KEY = "my-app:";


    /**
     * 用户token缓存key
     */
    String USER_TOKEN_KEY = REDIS_APP_KEY + "user_token:";


    /**
     * 用户token缓存 过期时间,三十分钟
     */
    Long USER_TOKEN_EXPIRE = 60L * 30L;
}
