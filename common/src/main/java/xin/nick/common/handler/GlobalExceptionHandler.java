package xin.nick.common.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import xin.nick.common.entity.Result;
import xin.nick.common.entity.ResultCode;
import xin.nick.common.exception.MyException;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Optional;

/**
 * 全局异常拦截
 * @author Nick
 * @since 2022/7/21/021
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {


    @Autowired
    private MessageSource messageSource;

    /**
     * 登录认证异常
     */
    @ExceptionHandler(InsufficientAuthenticationException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Result<String> handleInsufficientAuthenticationException(InsufficientAuthenticationException e) {
        return Result.error(ResultCode.UNAUTHORIZED);
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Result<String> handleNoHandlerFoundException(NoHandlerFoundException e) {
        return Result.error(ResultCode.NOT_FOUND);
    }

    @ExceptionHandler(InternalAuthenticationServiceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result<String> handleInternalAuthenticationServiceException(InternalAuthenticationServiceException e) {
        return Result.error(ResultCode.PARAM_ERROR, e.getMessage());
    }


    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result<String> handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        return Result.error(ResultCode.REQUEST_METHOD_ERROR);
    }

    /**
     * 参数类型异常
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result<String> handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        log.error(e.getMessage(), e);
        return Result.error(ResultCode.PARAM_FORMAT_ERROR);
    }

    /**
     * 参数类型缺失
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result<String> handleMissingServletRequestParameterException(MissingServletRequestParameterException e) {
        log.error(e.getMessage(), e);
        return Result.error(ResultCode.PARAM_EMPTY_ERROR);
    }

    /**
     * 请求的媒体类型不支持
     */
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public Result<String> handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException e) {
        log.warn("请求的媒体类型不支持");
        return Result.error(ResultCode.PARAM_NOT_SUPPORTED);
    }

    /**
     * MultipartException
     */
    @ExceptionHandler(MultipartException.class)
    public Result<String> handleMultipartException(MultipartException e) {
        log.warn("多参数上传出错: {}", e.getMessage());
        return Result.error(ResultCode.PARAM_FORMAT_ERROR);
    }

    /**
     * MissingServletRequestPartException
     */
    @ExceptionHandler(MissingServletRequestPartException.class)
    public Result<String> handleMissingServletRequestPartException(MissingServletRequestPartException e) {
        log.warn("参数缺失: {}", e.getMessage());
        log.error("参数缺失: ", e);
        return Result.error(ResultCode.PARAM_EMPTY_ERROR);
    }

    /**
     * 自定义验证异常
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result<String> validExceptionHandler(MethodArgumentNotValidException e) {
        log.error(e.getMessage(), e);
        String defaultMessage = Optional.of(e)
                .map(MethodArgumentNotValidException::getBindingResult)
                .map(BindingResult::getFieldError)
                .map(FieldError::getDefaultMessage)
                .orElse(ResultCode.PARAM_ERROR.getDescription());
        return Result.error(defaultMessage);
    }

    //


    /**
     * AccessDeniedException
     */
    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Result<String> handleAccessDeniedException(AccessDeniedException e) {
        log.error("用户无权限");
        return Result.error(ResultCode.FORBIDDEN);
    }


    /**
     * 自定义业务异常
     * @param e 异常
     * @return 返回规范的异常信息
     */
    @ExceptionHandler(MyException.class)
    public Result<String> handleMyException(MyException e) {
        log.error(e.getMessage(), e);
        String message = getMessage(e);
        return Result.error(e.getCode(), message);
    }

    /**
     * 获取国际化消息
     *
     * @param e 异常
     * @return 返回异常消息
     */
    public String getMessage(MyException e) {
        String code = "response." + e.getCode();
        String message = e.getMessage();
        try {
            // i18n 没有配置
            // message = messageSource.getMessage(code, new String[]{}, Locale.getDefault());
            if (message == null || message.isEmpty()) {
                message = e.getMessage();
            }
        } catch (Exception exception) {
            log.error("i18n异常：" , exception);
        }

        return message;

    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Result<String> handleException(Exception e) {
        log.error("Exception", e);
        return Result.error();
    }

}
