package xin.nick;

import org.apache.poi.hwpf.converter.WordToHtmlConverter;
import org.apache.poi.hwpf.usermodel.Picture;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.util.Base64;

/**
 * @author Nick
 * @since 2022/6/22/022
 */
public class ImageConverter extends WordToHtmlConverter {

    public ImageConverter(Document document) {
        super(document);
    }
    @Override
    protected void processImageWithoutPicturesManager(Element currentBlock, boolean inlined, Picture picture){
        Element imgNode = currentBlock.getOwnerDocument().createElement("img");
        StringBuffer sb = new StringBuffer();
        sb.append(Base64.getMimeEncoder().encodeToString(picture.getRawContent()));
        sb.insert(0, "data:" + picture.getMimeType() + ";base64,");
        imgNode.setAttribute("src", sb.toString());
        currentBlock.appendChild(imgNode);
    }
}
