
-- ----------------------------
-- Records of t_system_user
-- ----------------------------
INSERT INTO `t_system_user` VALUES (1, 'nick', '$2a$10$fUq4qUl67VmHTa0pbpG6CO2jVnGsSavClFNs/zFQzZGlEoc/OxgQi', 'Nick', '13141234567', 'https://profile-avatar.csdnimg.cn/9f8584fadd944a348974c258181e8ffe_weixin_45508768.jpg', 0);
INSERT INTO `t_system_user` VALUES (2, 'admin', '$2a$10$6koznlHCnAGhwq0Cq34eGuT/34ZzAfoPfR00cxdO7Lqs2iFI0BCu6', '超级管理员', '18888888888', 'https://www.upyun.com/static/img/beian.d0289dc.png', 0);

