package xin.nick;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.client.RestTemplate;
import xin.nick.component.DemoComponent;
import xin.nick.entity.Role;
import xin.nick.manager.RoleManager;

import javax.annotation.Resource;
import java.util.List;

// SpringBootTest.WebEnvironment.RANDOM_PORT
// 设置随机端口启动服务器（有助于避免测试环境中的冲突）
@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoApplicationTest {

    //使用@LocalServerPort将端口注入进来
    @Value("${local.server.port}")
    private int port;

    @Resource
    DemoComponent demoComponent;

    @Resource
    RoleManager roleManager;

    @Resource
    RestTemplateBuilder restTemplateBuilder;

    @Test
    void test() {
        log.info("三十岁");
    }


    @Test
    void test1() {
        System.out.println(demoComponent);
    }

    @Test
    void greetingShouldReturnDefaultMessage() {
        log.info("请求 /sayHello");
        RestTemplate restTemplate = restTemplateBuilder.build();
        String result = restTemplate.getForObject("http://localhost:" + port + "/sayHello", String.class);
        log.info("请求结果: {}", result);
    }

    public static void main(String[] args) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String nick = bCryptPasswordEncoder.encode("nick");
        String pp = bCryptPasswordEncoder.encode("111111");
        System.out.println(nick);
        System.out.println(pp);
    }

}
