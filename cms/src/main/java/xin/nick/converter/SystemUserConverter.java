package xin.nick.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import xin.nick.domain.dto.SystemUserAddDTO;
import xin.nick.domain.dto.SystemUserEditDTO;
import xin.nick.domain.vo.SystemUserVO;
import xin.nick.entity.SystemUser;

import java.util.List;

/**
 * @author Nick
 * @date 2022/8/4
 */
@Mapper
public interface SystemUserConverter {

    SystemUserConverter INSTANCE = Mappers.getMapper(SystemUserConverter.class);

    /**
     * bo(do) --> vo
     *
     * @param systemUser
     * @return
     */
    SystemUserVO systemUserToSystemUserVo(SystemUser systemUser);

    /**
     * bo(do)list --> vo list
     *
     * @param systemUserList
     * @return
     */
    List<SystemUserVO> systemUserListToSystemUserVoList(List<SystemUser> systemUserList);

    /**
     * bo(do)page --> vo page
     *
     * @param systemUserList
     * @return
     */
    Page<SystemUserVO> systemUserPageToSystemUserVoPage(Page<SystemUser> systemUserList);


    /**
     * 转 bo(do)
     * @param systemUserAddDTO
     * @return
     */
    SystemUser toSystemUser(SystemUserAddDTO systemUserAddDTO);

    /**
     * 转 bo(do)
     * @param systemUserEditDTO
     * @return
     */
    SystemUser toSystemUser(SystemUserEditDTO systemUserEditDTO);
}
