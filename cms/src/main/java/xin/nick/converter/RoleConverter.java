package xin.nick.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import xin.nick.domain.dto.RoleAddDTO;
import xin.nick.domain.dto.RoleEditDTO;
import xin.nick.domain.dto.SystemUserAddDTO;
import xin.nick.domain.vo.RoleDetailVO;
import xin.nick.domain.vo.RoleVO;
import xin.nick.domain.vo.SystemUserVO;
import xin.nick.entity.Role;
import xin.nick.entity.SystemUser;

import java.util.List;

/**
 * 角色转换
 * @author Nick
 * @date 2022/8/4
 */
@Mapper
public interface RoleConverter {

    RoleConverter INSTANCE = Mappers.getMapper(RoleConverter.class);

    /**
     * bo(do) --> vo
     *
     * @param role
     * @return
     */
    RoleVO roleToRoleVo(Role role);

    /**
     * bo(do) --> vo
     *
     * @param role
     * @return
     */
    RoleDetailVO roleToRoleDetailVO(Role role);

    /**
     * bo(do) --> vo
     *
     * @param roleAddDTO
     * @return
     */
    Role toRole(RoleAddDTO roleAddDTO);
    /**
     * bo(do) --> vo
     *
     * @param roleEditDTO
     * @return
     */
    Role toRole(RoleEditDTO roleEditDTO);

    /**
     * bo(do)list --> vo list
     *
     * @param roleList
     * @return
     */
    List<RoleVO> roleListToRoleVoList(List<Role> roleList);

    /**
     * bo(do)page --> vo page
     *
     * @param rolePage
     * @return
     */
    Page<RoleVO> rolePageToRoleVoPage(Page<Role> rolePage);


}
