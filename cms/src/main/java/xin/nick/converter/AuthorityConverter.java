package xin.nick.converter;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import xin.nick.domain.dto.AuthorityAddDTO;
import xin.nick.domain.dto.AuthorityEditDTO;
import xin.nick.domain.dto.RoleAddDTO;
import xin.nick.domain.dto.RoleEditDTO;
import xin.nick.domain.vo.AuthorityVO;
import xin.nick.domain.vo.RoleDetailVO;
import xin.nick.domain.vo.RoleVO;
import xin.nick.entity.Authority;
import xin.nick.entity.Role;

import java.util.List;

/**
 * 角色转换
 * @author Nick
 * @date 2022/8/4
 */
@Mapper
public interface AuthorityConverter {

    AuthorityConverter INSTANCE = Mappers.getMapper(AuthorityConverter.class);

    /**
     * bo(do) --> vo
     *
     * @param authority
     * @return
     */
    AuthorityVO toVo(Authority authority);

    /**
     * bo(do)list --> vo list
     *
     * @param authorityList
     * @return
     */
    List<AuthorityVO> toVoList(List<Authority> authorityList);

    /**
     * bo(do)page --> vo page
     *
     * @param authorityPage
     * @return
     */
    Page<Authority> toVoPage(Page<Authority> authorityPage);


    /**
     * to do
     * @param authorityAddDTO
     * @return
     */
    Authority toBo(AuthorityAddDTO authorityAddDTO);

    /**
     * to do
     * @param authorityEditDTO
     * @return
     */
    Authority toBo(AuthorityEditDTO authorityEditDTO);
}
