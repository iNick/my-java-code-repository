package xin.nick.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author Nick
 * @since 2022-07-27
 */
@Getter
@Setter
@TableName("t_system_user")
public class SystemUser {

    @Schema(description = "user_id")
    @TableId(value = "user_id", type = IdType.ASSIGN_ID)
    private Long userId;

    @Schema(description = "账号")
    @TableField("account")
    private String account;

    @Schema(description = "密码")
    @TableField("password")
    private String password;

    @Schema(description = "用户名")
    @TableField("username")
    private String username;

    @Schema(description = "手机")
    @TableField("tel_number")
    private String telNumber;

    @Schema(description = "头像地址")
    @TableField("avatar")
    private String avatar;

    @Schema(description = "是否删除")
    @TableField("deleted")
    @TableLogic
    private Boolean deleted;

}
