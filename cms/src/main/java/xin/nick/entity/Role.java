package xin.nick.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * <p>
 * 角色
 * </p>
 *
 * @author Nick
 * @since 2022-08-09
 */
@Getter
@Setter
@TableName("t_role")
@Schema(description = "Role对象")
@ToString
public class Role {

    @Schema(description = "角色id")
    @TableId(value = "role_id", type = IdType.AUTO)
    private Long roleId;

    @Schema(description = "角色Key")
    @TableField("role_key")
    private String roleKey;

    @Schema(description = "角色名字")
    @TableField("role_name")
    private String roleName;

    @Schema(description = "是否禁用")
    @TableField("disabled")
    private Boolean disabled;

    @Schema(description = "是否删除")
    @TableField("deleted")
    @TableLogic
    private Boolean deleted;


}
