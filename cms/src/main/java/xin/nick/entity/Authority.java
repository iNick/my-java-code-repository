package xin.nick.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * <p>
 * 权限表
 * </p>
 *
 * @author Nick
 * @since 2022-08-09
 */
@Data
@TableName("t_authority")
@Schema(description = "Authority对象")
public class Authority {

    @Schema(description = "权限id")
    @TableId(value = "authority_id", type = IdType.AUTO)
    private Long authorityId;

    @Schema(description = "权限key")
    @TableField("authority_key")
    private String authorityKey;

    @Schema(description = "权限名字")
    @TableField("authority_name")
    private String authorityName;

    @Schema(description = "权限对应url")
    @TableField("authority_url")
    private String authorityUrl;

    @Schema(description = "权限是否禁用")
    @TableField("disabled")
    private Boolean disabled;

    @Schema(description = "是否删除")
    @TableField("deleted")
    @TableLogic
    private Boolean deleted;


}
