package xin.nick.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 角色权限关联
 * </p>
 *
 * @author Nick
 * @since 2022-08-09
 */
@Getter
@Setter
@TableName("t_role_authority")
@Schema(description = "RoleAuthority对象")
public class RoleAuthority {

    @Schema(description = "角色权限id")
    @TableId(value = "role_authority_id", type = IdType.AUTO)
    private Long roleAuthorityId;

    @Schema(description = "角色id")
    @TableField("role_id")
    private Long roleId;

    @Schema(description = "权限id")
    @TableField("authority_id")
    private Long authorityId;

    @Schema(description = "是否删除")
    @TableField("deleted")
    @TableLogic
    private Boolean deleted;


}
