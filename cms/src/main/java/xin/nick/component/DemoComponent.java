package xin.nick.component;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author Nick
 * @since 2022/1/22
 */
@Data
@Component
public class DemoComponent {
    /**
     * 测试,名字
     */
    private String name;
}
