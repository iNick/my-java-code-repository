package xin.nick.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * 放行白名单配置
 *
 * @author Nick
 */
@Configuration
@ConfigurationProperties(prefix = "ignore")
@Data
public class IgnoreWhiteProperties {
    /**
     * 放行白名单配置，不校验此处的白名单
     */
    private List<String> whites = new ArrayList<>();

    /**
     * 需要校验的路径
     */
    private List<String> auths = new ArrayList<>();

}
