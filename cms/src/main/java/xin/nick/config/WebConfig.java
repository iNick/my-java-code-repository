package xin.nick.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Nick
 * @since 2022/1/22
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {


    /**
     * 文件上传的目录
     */
    @Value("${nick.file-upload.path}")
    private String fileUploadPath;

    /**
     * 文件请求地址
     */
    @Value("${nick.file-upload.request-path}")
    private String fileRequestPath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(fileRequestPath + "/**")
                .addResourceLocations("file:" + fileUploadPath);
    }

}
