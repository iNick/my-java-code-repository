package xin.nick.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author Nick
 * @date 2022/11/24
 */
@Data
@Component
@ConfigurationProperties(prefix = "doc-info")
public class DocInfo {

    private String title = "后台管理系统";
    private String description = "后台管理系统v1.20221124";
    private String version = "v1.20221124";
    private String websiteName = "网址";
    private String websiteUrl = "https://www.xxx.com";
}