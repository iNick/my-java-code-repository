package xin.nick.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * 文件处理
 * @author Nick
 * @since 2022/2/10
 */
public interface IFileService {

    /**
     * 保存文件
     * @param file
     * @return
     */
    String uploadFile(MultipartFile file);

}
