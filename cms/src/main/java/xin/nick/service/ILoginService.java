package xin.nick.service;

import xin.nick.domain.vo.LoginUserVO;

/**
 * @author Nick
 * @since 2022/7/26/026
 */
public interface ILoginService {

    /**
     * 登录
     * @param account
     * @param password
     * @return
     */
    LoginUserVO login(String account, String password);


    /**
     * 退出登录
     */
    void logout();
}
