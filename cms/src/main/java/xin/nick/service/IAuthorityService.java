package xin.nick.service;

import xin.nick.entity.Authority;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author Nick
 * @since 2022-08-09
 */
public interface IAuthorityService extends IService<Authority> {

}
