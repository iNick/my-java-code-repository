package xin.nick.service;

import xin.nick.domain.dto.RoleSetAuthorityListDTO;
import xin.nick.domain.vo.AuthorityVO;
import xin.nick.domain.vo.RoleSetAuthorityListVO;
import xin.nick.entity.RoleAuthority;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色权限关联 服务类
 * </p>
 *
 * @author Nick
 * @since 2022-08-10
 */
public interface IRoleAuthorityService extends IService<RoleAuthority> {

    /**
     * 根据角色id获取权限列表
     * @param roleId
     * @return
     */
    List<AuthorityVO> getAuthorityListByRoleId(Long roleId);

    /**
     * 设置角色的权限信息
     * @param roleSetAuthorityListDTO
     * @return
     */
    RoleSetAuthorityListVO setRoleAuthorityList(RoleSetAuthorityListDTO roleSetAuthorityListDTO);
}
