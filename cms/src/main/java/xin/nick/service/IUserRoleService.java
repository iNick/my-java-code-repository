package xin.nick.service;

import xin.nick.domain.dto.UserSetRoleListDTO;
import xin.nick.domain.vo.UserSetRoleListVO;
import xin.nick.entity.Role;
import xin.nick.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户角色关联 服务类
 * </p>
 *
 * @author Nick
 * @since 2022-08-10
 */
public interface IUserRoleService extends IService<UserRole> {

    /**
     * 根据用户id获取角色列表
     * @param userId
     * @return
     */
    List<Role> getRoleListByUserId(Long userId);

    /**
     * 设置用户角色列表
     * @param userSetRoleListDTO
     * @return
     */
    UserSetRoleListVO setUserRoleList(UserSetRoleListDTO userSetRoleListDTO);
}
