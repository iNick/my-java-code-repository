package xin.nick.service.impl;

import cn.hutool.core.util.IdUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xin.nick.common.constant.SystemConstants;
import xin.nick.common.entity.LoginUser;
import xin.nick.common.entity.ResultCode;
import xin.nick.common.util.MyAssert;
import xin.nick.common.util.SecurityUtil;
import xin.nick.common.util.ServletUtil;
import xin.nick.domain.vo.LoginUserVO;
import xin.nick.service.ILoginService;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author Nick
 * @since 2022/7/26/026
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class LoginServiceImpl implements ILoginService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public LoginUserVO login(String account, String password) {
        LoginUserVO loginUserVO = new LoginUserVO();

        // 验证密码
        // 用户验证
        Authentication authentication = null;
        try {
            // 该方法会去调用 CustomUserDetailsService.loadUserByUsername
            authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(account, password));
        } catch (Exception e) {
            MyAssert.ifInstanceOfThrow(BadCredentialsException.class, e, ResultCode.ACCOUNT_OR_PASSWORD_ERROR);
            MyAssert.throwException(e.getMessage());
        }

        // authentication 不会返回为空,如果为空,会直接跳异常,而不会返回
        // 强迫症,还是多加个判断吧
        MyAssert.notNull(authentication, ResultCode.ACCOUNT_OR_PASSWORD_ERROR);
        LoginUser user = (LoginUser) authentication.getPrincipal();
        MyAssert.notNull(user, ResultCode.ACCOUNT_OR_PASSWORD_ERROR);

        // 登录成功之后,设置token
        // token 可以试试用JWT
        String token = IdUtil.fastSimpleUUID();
        user.setToken(token);

        // 设置Redis登录信息,
        String tokenKey = SystemConstants.USER_TOKEN_KEY + token;
        redisTemplate.opsForValue().set(tokenKey, user);
        redisTemplate.expire(tokenKey, SystemConstants.USER_TOKEN_EXPIRE, TimeUnit.SECONDS);

        // 返回对象
        loginUserVO.setToken(token);
        loginUserVO.setUserId(user.getUserId());
        loginUserVO.setUsername(user.getNickname());
        loginUserVO.setAccount(account);

        return loginUserVO;
    }

    @Override
    public void logout() {
        // 处理用户缓存
        String token = ServletUtil.getToken();
//        LoginUser currentUser = SecurityUtil.getCurrentUser();
//        String token = currentUser.getToken();
        String tokenKey = SystemConstants.USER_TOKEN_KEY + token;
        if (Objects.nonNull(redisTemplate.opsForValue().get(tokenKey))) {
            redisTemplate.delete(tokenKey);
        }
    }
}
