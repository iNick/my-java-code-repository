package xin.nick.service.impl;

import org.springframework.transaction.annotation.Transactional;
import xin.nick.entity.Authority;
import xin.nick.mapper.AuthorityMapper;
import xin.nick.service.IAuthorityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author Nick
 * @since 2022-08-09
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class AuthorityServiceImpl extends ServiceImpl<AuthorityMapper, Authority> implements IAuthorityService {

}
