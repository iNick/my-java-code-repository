package xin.nick.service;

import xin.nick.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author Nick
 * @since 2022-08-09
 */
public interface IRoleService extends IService<Role> {

    /**
     * 根据用户id获取角色key列表
     * @param userId
     * @return
     */
    List<String> getRoleKeyListByUserId(Long userId);

    /**
     * 根据用户id获取角色列表
     * @param userId
     * @return
     */
    List<Role> getRoleListByUserId(Long userId);
}
