package xin.nick.service;

import xin.nick.domain.dto.SystemUserAddDTO;
import xin.nick.domain.dto.SystemUserEditDTO;
import xin.nick.domain.vo.SystemUserDetailVO;
import xin.nick.entity.SystemUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author Nick
 * @since 2022-07-27
 */
public interface ISystemUserService extends IService<SystemUser> {

    /**
     * 添加用户
     * @param systemUserAddDTO
     * @return
     */
    SystemUser addUser(SystemUserAddDTO systemUserAddDTO);

    /**
     * 修改用户
     * @param systemUserEditDTO
     * @return
     */
    SystemUser editUser(SystemUserEditDTO systemUserEditDTO);

    /**
     * 删除用户
     * @param userId
     */
    void delete(Long userId);

    /**
     * 批量删除用户
     * @param userIdList
     */
    void deleteByIdList(List<Long> userIdList);
}
