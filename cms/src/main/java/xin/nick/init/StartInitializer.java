package xin.nick.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import xin.nick.manager.SystemUserManager;
import xin.nick.service.ISystemUserService;

/**
 * @author Nick
 * @date 2022/8/3
 */
@Component
@Order(1)
public class StartInitializer implements CommandLineRunner {

    @Autowired
    private SystemUserManager systemUserManager;

    @Override
    public void run(String... args) {
        systemUserManager.checkNickAndInsert();
    }
}
