package xin.nick.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author Nick
 * @date 2022/8/11
 */
@Data
public class AuthorityAddDTO {

    @Schema(description = "权限key")
    private String authorityKey;

    @Schema(description = "权限名字")
    private String authorityName;

    @Schema(description = "权限url")
    private String authorityUrl;

    @Schema(description = "是否禁用")
    private Boolean disabled;
}
