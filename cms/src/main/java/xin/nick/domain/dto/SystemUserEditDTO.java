package xin.nick.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 用户新增对象
 * @author Nick
 * @date 2022/8/10
 */
@Data
public class SystemUserEditDTO {


    @Schema(description = "用户id")
    @NotNull(message = "[account] 不可为空")
    private Long userId;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "账号")
    private String account;

    @Schema(description = "密码")
    private String password;

    @Schema(description = "手机号")
    private String telNumber;

    @Schema(description = "用户头像")
    private String avatar;

}
