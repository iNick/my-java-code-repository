package xin.nick.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Nick
 * @date 2022/8/10
 */
@Data
public class RoleEditDTO {

    @Schema(description = "角色id")
    @NotNull(message = "[roleId] 不可为空")
    private Long roleId;

    @Schema(description = "父级id")
    private Long parentId;

    @Schema(description = "角色Key")
    private String roleKey;

    @Schema(description = "角色名字")
    private String roleName;

    @Schema(description = "是否禁用")
    private Boolean disabled;

}
