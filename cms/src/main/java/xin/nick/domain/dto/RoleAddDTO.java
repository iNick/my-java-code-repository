package xin.nick.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author Nick
 * @date 2022/8/10
 */
@Data
public class RoleAddDTO {

    @Schema(description = "角色Key")
    @NotBlank(message = "[roleKey] 不可为空")
    private String roleKey;

    @Schema(description = "父级id")
    private Long parentId;

    @Schema(description = "角色名字")
    @NotBlank(message = "[roleName] 不可为空")
    private String roleName;


}
