package xin.nick.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 设置用户的角色id列表
 * @author Nick
 * @date 2022/8/11
 */
@Data
public class UserSetRoleListDTO {

    @Schema(description = "用户id")
    @NotNull(message = "[userId] 不可为空")
    private Long userId;

    @Schema(description = "角色id列表")
    private List<Long>  roleIdList;
}
