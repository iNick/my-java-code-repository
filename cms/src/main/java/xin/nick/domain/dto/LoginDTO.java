package xin.nick.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 用户登录对象
 * @author Nick
 * @since 2022/7/26/026
 */
@Data
public class LoginDTO {

    /**
     * 账号
     */
    @Schema(description = "账号")
    @NotBlank(message = "[account] 不可为空")
    private String account;

    /**
     * 用户密码
     */
    @Schema(description = "密码")
    @NotBlank(message = "[password] 不可为空")
    private String password;

    /**
     * 请求验证码对应UUID
     */
    @Schema(description = "uuid")
    private String uuid;

    /**
     * 验证码
     */
    @Schema(description = "验证码")
    private String code;

}
