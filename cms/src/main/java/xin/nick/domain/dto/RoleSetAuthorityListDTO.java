package xin.nick.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 设置角色的权限列表
 * @author Nick
 * @date 2022/8/11
 */
@Data
public class RoleSetAuthorityListDTO {

    @Schema(description = "角色id")
    @NotNull(message = "[roleId] 不可为空")
    private Long roleId;

    @Schema(description = "权限id列表")
    private List<Long>  authorityIdList;
}
