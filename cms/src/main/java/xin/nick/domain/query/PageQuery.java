package xin.nick.domain.query;


import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Objects;

/**
 * 分页查询对象
 * @author Nick
 * @since 2022/2/10
 */
@Schema( description = "分页参数")
public class PageQuery {

    /**
     * 页面大小
     */
    @Schema(description = "页面大小,默认为10")
    private Long size;

    /**
     * 当前页面
     */
    @Schema(description = "当前页面,默认为1")
    private Long current;

    public Long getSize() {
        if (Objects.isNull(size)) {
            size = 10L;
        }
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public Long getCurrent() {
        if (Objects.isNull(current)) {
            current = 1L;
        }
        return current;
    }

    public void setCurrent(long current) {
        this.current = current;
    }
}
