package xin.nick.domain.query;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;


/**
 * @author Nick
 * @date 2022/8/3
 */
@Data
public class SystemUserQuery extends PageQuery {

    @Schema(description = "账号")
    private String account;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "手机")
    private String telNumber;

}
