package xin.nick.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * 用户设置权限列表VO
 * @author Nick
 * @date 2022/8/11
 */
@Data
public class UserSetRoleListVO {

    @Schema(description = "用户信息")
    private SystemUserVO user;

    @Schema(description = "角色列表")
    private List<RoleVO> roleList;

}
