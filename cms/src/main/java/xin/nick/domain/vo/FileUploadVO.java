package xin.nick.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.experimental.SuperBuilder;

/**
 * @author Nick
 * @date 2022/8/20
 */
@Data
@SuperBuilder
public class FileUploadVO {

    @Schema(description = "文件url")
    private String fileUrl;

}

