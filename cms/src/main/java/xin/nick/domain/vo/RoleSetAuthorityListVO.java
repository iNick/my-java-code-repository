package xin.nick.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * @author Nick
 * @date 2022/8/11
 */
@Data
public class RoleSetAuthorityListVO {


    @Schema(description = "角色信息")
    private RoleVO role;

    @Schema(description = "权限列表")
    private List<AuthorityVO> authorityList;

}
