package xin.nick.domain.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * 系统用户
 * @author Nick
 * @date 2022/8/4
 */
@Data
public class SystemUserDetailVO extends SystemUserVO{

    @Schema(description = "角色列表")
    private List<RoleVO> roleList;

}
