package xin.nick.domain.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author Nick
 * @date 2022/8/4
 */
@Data
public class SystemUserVO {

    @Schema(description = "用户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    @Schema(description = "账号")
    private String account;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "手机")
    private String telNumber;

    @Schema(description = "头像地址")
    private String avatar;
}
