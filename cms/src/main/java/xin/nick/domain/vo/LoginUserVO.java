package xin.nick.domain.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * 登录成功后用户vo
 * @author Nick
 * @since 2022/7/27/027
 */
@Data
public class LoginUserVO {

    @Schema(description = "用户id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

    @Schema(description = "账号")
    private String account;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "token")
    private String token;

    @Schema(description = "权限列表")
    private List<String> authorityList;

}
