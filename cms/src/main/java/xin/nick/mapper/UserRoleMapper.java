package xin.nick.mapper;

import org.apache.ibatis.annotations.Mapper;
import xin.nick.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户角色关联 Mapper 接口
 * </p>
 *
 * @author Nick
 * @since 2022-08-09
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
