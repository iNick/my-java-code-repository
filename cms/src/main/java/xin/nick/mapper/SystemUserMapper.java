package xin.nick.mapper;

import org.apache.ibatis.annotations.Mapper;
import xin.nick.entity.SystemUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author Nick
 * @since 2022-07-27
 */
@Mapper
public interface SystemUserMapper extends BaseMapper<SystemUser> {

}
