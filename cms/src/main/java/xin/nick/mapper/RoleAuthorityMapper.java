package xin.nick.mapper;

import org.apache.ibatis.annotations.Mapper;
import xin.nick.entity.RoleAuthority;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色权限关联 Mapper 接口
 * </p>
 *
 * @author Nick
 * @since 2022-08-09
 */
@Mapper
public interface RoleAuthorityMapper extends BaseMapper<RoleAuthority> {

}
