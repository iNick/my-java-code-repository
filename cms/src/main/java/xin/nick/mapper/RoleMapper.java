package xin.nick.mapper;

import org.apache.ibatis.annotations.Mapper;
import xin.nick.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author Nick
 * @since 2022-08-09
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

}
