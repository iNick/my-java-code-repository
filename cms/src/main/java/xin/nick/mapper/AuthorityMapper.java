package xin.nick.mapper;

import org.apache.ibatis.annotations.Mapper;
import xin.nick.entity.Authority;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author Nick
 * @since 2022-08-09
 */
@Mapper
public interface AuthorityMapper extends BaseMapper<Authority> {

}
