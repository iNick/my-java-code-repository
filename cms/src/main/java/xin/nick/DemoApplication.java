package xin.nick;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PreDestroy;

/**
 *
 * @author Nick
 */
@MapperScan("xin.nick.mapper")
@SpringBootApplication
@Slf4j
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
        log.info("项目启动! project is running!");
    }

    @PreDestroy
    public void destroy() {
        log.info("项目退出~ project exit~");
    }
}
