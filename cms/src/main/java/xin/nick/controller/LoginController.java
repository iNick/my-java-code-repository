package xin.nick.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.domain.dto.LoginDTO;
import xin.nick.domain.vo.LoginUserVO;
import xin.nick.service.ILoginService;

import javax.validation.Valid;

/**
 * @author Nick
 * @since 2022/7/26/026
 */
@Tag(name = "登录模块")
@RestController
@RequestMapping("/api")
public class LoginController {

    @Autowired
    private ILoginService loginService;

    @Operation(summary = "登录")
    @PostMapping("/login")
    public LoginUserVO login(@RequestBody @Valid LoginDTO loginDTO) {

        return loginService.login(loginDTO.getAccount(), loginDTO.getPassword());
    }

    @Operation(summary = "退出登录")
    @DeleteMapping("/logout")
    public void logout() {
        loginService.logout();
    }

}
