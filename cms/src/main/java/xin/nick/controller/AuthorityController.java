package xin.nick.controller;


import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.common.util.ServletUtil;
import xin.nick.converter.AuthorityConverter;
import xin.nick.domain.dto.AuthorityAddDTO;
import xin.nick.domain.dto.AuthorityEditDTO;
import xin.nick.domain.vo.AuthorityVO;
import xin.nick.entity.Authority;
import xin.nick.service.IAuthorityService;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 权限表 前端控制器
 * </p>
 *
 * @author Nick
 * @since 2022-08-09
 */
@Tag(name = "权限管理")
@RestController
@RequestMapping("/api/authority")
public class AuthorityController {

    @Autowired
    private IAuthorityService authorityService;

    @Operation(summary = "详情")
    @GetMapping("/getInfo/{authorityId}")
    public AuthorityVO detail(@PathVariable Long authorityId) {
        Authority authority = authorityService.getById(authorityId);
        return AuthorityConverter.INSTANCE.toVo(authority);
    }

    @Operation(summary = "列表")
    @GetMapping("/list")
    public List<AuthorityVO> list() {
        List<Authority> list = authorityService.list(Wrappers.<Authority>lambdaQuery().orderByDesc(Authority::getAuthorityId));
        return AuthorityConverter.INSTANCE.toVoList(list);
    }

    @Operation(summary = "分页")
    @GetMapping("/page")
    public Page<AuthorityVO> page() {
        Page page = authorityService.page(ServletUtil.getQueryPage(), Wrappers.<Authority>lambdaQuery().orderByDesc(Authority::getAuthorityId));
        return AuthorityConverter.INSTANCE.toVoPage(page);
    }

    @Operation(summary = "添加")
    @PostMapping("/add")
    public AuthorityVO add(@RequestBody @Valid AuthorityAddDTO authorityAddDTO) {
        Authority authority = AuthorityConverter.INSTANCE.toBo(authorityAddDTO);
        authorityService.save(authority);
        return detail(authority.getAuthorityId());
    }

    @Operation(summary = "编辑")
    @PostMapping("/edit")
    public AuthorityVO edit(@RequestBody @Valid AuthorityEditDTO authorityEditDTO) {
        Authority authority = AuthorityConverter.INSTANCE.toBo(authorityEditDTO);
        authorityService.updateById(authority);
        return detail(authority.getAuthorityId());
    }

    @Operation(summary = "删除")
    @DeleteMapping("/delete/{authorityId}")
    public void delete(@PathVariable Long authorityId) {
        authorityService.removeById(authorityId);
    }

    @Operation(summary = "批量删除")
    @DeleteMapping("/deleteByIdList/{authorityIdList}")
    public void delete(@PathVariable List<Long> authorityIdList) {
        if (CollectionUtils.isEmpty(authorityIdList)) {
            return;
        }
        authorityService.removeByIds(authorityIdList);
    }


}
