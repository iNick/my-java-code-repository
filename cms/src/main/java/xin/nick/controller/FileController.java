package xin.nick.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import xin.nick.common.constant.ResultCode;
import xin.nick.common.util.MyAssert;
import xin.nick.domain.vo.FileUploadVO;
import xin.nick.service.IFileService;

import java.util.Objects;

/**
 * 文件管理
 * @author Nick
 * @since 2022/2/10
 */
@Tag(name = "文件管理")
@RestController
@RequestMapping("/api/file")
public class FileController {

    @Autowired
    private IFileService fileService;

    @Operation(summary = "普通文件上传")
    @PostMapping(value = "/uploadFile")
    public FileUploadVO fileUpload(@RequestPart MultipartFile file) {

        //判断文件是否为空
        if(Objects.isNull(file) || file.isEmpty()){
            MyAssert.throwException(ResultCode.FILE_EMPTY);
        }

        String uploadFile = fileService.uploadFile(file);
        FileUploadVO build = FileUploadVO.builder().fileUrl(uploadFile).build();
        return build;

    }

}
