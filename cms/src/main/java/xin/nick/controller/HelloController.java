package xin.nick.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.common.annotation.AopLog;
import xin.nick.common.entity.Result;

/**
 * @author Nick
 */
@Tag(name = "Hello管理")
@Slf4j
@RestController("/api")
public class HelloController {


    @Operation(summary = "说你好")
    @GetMapping("/sayHello")
    public String sayHello() {
        log.info("say hello");
        return "Hello";
    }

    @Operation(summary = "sayHell2")
    @AopLog
    @GetMapping("/sayHell2")
    public Result sayHell2() {
        log.info("say Result");
        return Result.success("Hello2");
    }

    @Operation(summary = "sayHell3")
    @AopLog
    @GetMapping("/sayHell3")
    public void sayHell3() {
        log.info("say Result");
    }

    @Operation(summary = "sayHell4")
    @AopLog
    @GetMapping("/sayHell4")
    public void sayHell4() {
        log.info("say Result");
        int i = 1 / 0;
    }

    @Operation(summary = "说你好2")
    @AopLog
    @GetMapping("/sayHello2")
    public String sayHello2() {
        log.info("say hello2");
        return "Hello2";
    }

    @Operation(summary = "主页面")
    @AopLog
    @GetMapping(value="/", produces="application/json;charset=UTF-8")
    public String index() {
        log.info("主页面");
        return "主页面";
    }


    @Operation(summary = "testRoleAdmin")
    @AopLog
    @GetMapping(value="/api/testRoleAdmin", produces="application/json;charset=UTF-8")
    public String testRoleAdmin() {
        log.info("我只有角色 admin 可以访问");
        return "我只有角色 admin 可以访问";
    }

}
