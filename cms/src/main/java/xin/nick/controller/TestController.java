package xin.nick.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Nick
 */
@Tag(name = "测试接口")
@Slf4j
@RestController
@RequestMapping("/api/test")
public class TestController {

    @Operation(summary = "authority1")
    @GetMapping("/authority1")
    public String authority1() {
        log.info("authority1");
        return "authority1";
    }

    @Operation(summary = "authority2")
    @GetMapping("/authority2")
    public String authority2() {
        log.info("authority2");
        return "authority2";
    }

    @Operation(summary = "authority3")
    @GetMapping("/authority3")
    public String authority3() {
        log.info("authority3");
        return "authority3";
    }

    @Operation(summary = "abab")
    @GetMapping("/abab")
    public String abab() {
        log.info("abab");
        return "abab";
    }

}
