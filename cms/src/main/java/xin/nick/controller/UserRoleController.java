package xin.nick.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.converter.RoleConverter;
import xin.nick.domain.dto.UserSetRoleListDTO;
import xin.nick.domain.vo.RoleVO;
import xin.nick.domain.vo.UserSetRoleListVO;
import xin.nick.entity.Role;
import xin.nick.service.IUserRoleService;

import java.util.List;

/**
 * <p>
 * 用户角色关联 前端控制器
 * </p>
 *
 * @author Nick
 * @since 2022-08-10
 */
@Tag(name = "用户角色管理")
@RestController
@RequestMapping("/api/user-role")
public class UserRoleController {

    @Autowired
    IUserRoleService userRoleService;

    /**
     * 根据用户id获取角色列表,
     * @param userId
     * @return
     */
    @Operation(summary = "根据用户id获取角色列表")
    @GetMapping("/getRoleListByUserId")
    public List<RoleVO> getRoleListByUserId(Long userId) {
        List<Role> roleList = userRoleService.getRoleListByUserId(userId);
        return RoleConverter.INSTANCE.roleListToRoleVoList(roleList);
    }


    /**
     * 设置用户角色列表
     * @param userSetRoleListDTO
     * @return
     */
    @Operation(summary = "设置用户角色列表")
    @GetMapping("/setUserRoleList")
    public UserSetRoleListVO setUserRoleList(@RequestBody UserSetRoleListDTO userSetRoleListDTO) {

        UserSetRoleListVO userSetRoleListVO = userRoleService.setUserRoleList(userSetRoleListDTO);
        return userSetRoleListVO;
    }

}
