package xin.nick.controller;


import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.common.util.ServletUtil;
import xin.nick.converter.RoleConverter;
import xin.nick.domain.dto.RoleAddDTO;
import xin.nick.domain.dto.RoleEditDTO;
import xin.nick.domain.vo.RoleDetailVO;
import xin.nick.domain.vo.RoleVO;
import xin.nick.entity.Role;
import xin.nick.service.IRoleService;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 角色 前端控制器
 * </p>
 *
 * @author Nick
 * @since 2022-08-09
 */
@Tag(name = "角色管理")
@RestController
@RequestMapping("/api/role")
public class RoleController {

    @Autowired
    private IRoleService roleService;

    @Operation(summary = "列表")
    @GetMapping("/list")
    public List<RoleVO> list() {
        List<Role> roleList = roleService.list();
        return RoleConverter.INSTANCE.roleListToRoleVoList(roleList);
    }

    @Operation(summary = "分页")
    @GetMapping("/page")
    public Page<RoleVO> page() {
        Page<Role> rolePage = roleService.page(ServletUtil.getQueryPage());
        return RoleConverter.INSTANCE.rolePageToRoleVoPage(rolePage);
    }

    @Operation(summary = "详情")
    @GetMapping("/detail/{roleId}")
    public RoleDetailVO detail(@PathVariable Long roleId) {
        Role role = roleService.getById(roleId);
        return RoleConverter.INSTANCE.roleToRoleDetailVO(role);
    }

    @Operation(summary = "新增")
    @PostMapping("/add")
    public RoleDetailVO add(@RequestBody @Valid RoleAddDTO roleAddDTO) {
        Role role = RoleConverter.INSTANCE.toRole(roleAddDTO);
        roleService.save(role);
        return detail(role.getRoleId());
    }

    @Operation(summary = "编辑")
    @PostMapping("/edit")
    public RoleDetailVO edit(@RequestBody @Valid RoleEditDTO roleEditDTO) {
        Role role = RoleConverter.INSTANCE.toRole(roleEditDTO);
        roleService.updateById(role);
        return detail(role.getRoleId());
    }

    @Operation(summary = "删除")
    @DeleteMapping("/delete/{roleId}")
    public void delete(@PathVariable Long roleId) {
        roleService.removeById(roleId);
    }

    @Operation(summary = "批量删除")
    @DeleteMapping("/deleteByIdList/{roleIdList}")
    public void delete(@PathVariable List<Long> roleIdList) {
        if (CollectionUtils.isEmpty(roleIdList)) {
            return;
        }
        roleService.removeByIds(roleIdList);
    }

}
