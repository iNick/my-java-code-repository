package xin.nick.controller;


import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.common.util.ServletUtil;
import xin.nick.converter.SystemUserConverter;
import xin.nick.domain.dto.SystemUserAddDTO;
import xin.nick.domain.dto.SystemUserEditDTO;
import xin.nick.domain.vo.SystemUserVO;
import xin.nick.entity.SystemUser;
import xin.nick.service.ISystemUserService;

import javax.validation.Valid;
import java.util.List;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author Nick
 * @since 2022-07-27
 */
@Tag(name = "系统用户管理")
@RestController
@RequestMapping("/api/system-user")
public class SystemUserController {


    @Autowired
    private ISystemUserService systemUserService;

    @Operation(summary = "详情")
    @GetMapping("/getInfo/{userId}")
    public SystemUserVO detail(@PathVariable Long userId) {
        SystemUser systemUser = systemUserService.getById(userId);
        return SystemUserConverter.INSTANCE.systemUserToSystemUserVo(systemUser);
    }

    @Operation(summary = "列表")
    @GetMapping("/list")
    public List<SystemUserVO> list() {
        List<SystemUser> list = systemUserService.list();
        return SystemUserConverter.INSTANCE.systemUserListToSystemUserVoList(list);
    }

    @Operation(summary = "分页")
    @GetMapping("/page")
    public Page<SystemUserVO> page() {
        Page page = systemUserService.page(ServletUtil.getQueryPage());
        return SystemUserConverter.INSTANCE.systemUserPageToSystemUserVoPage(page);
    }

    @Operation(summary = "添加")
    @PostMapping("/add")
    public SystemUserVO add(@RequestBody @Valid SystemUserAddDTO systemUserAddDTO) {
        SystemUser systemUser = systemUserService.addUser(systemUserAddDTO);
        return detail(systemUser.getUserId());
    }

    @Operation(summary = "编辑")
    @PutMapping("/edit")
    public SystemUserVO edit(@RequestBody @Valid SystemUserEditDTO systemUserEditDTO) {
        SystemUser systemUser = systemUserService.editUser(systemUserEditDTO);
        return detail(systemUser.getUserId());
    }

    @Operation(summary = "删除")
    @DeleteMapping("/delete/{userId}")
    public void delete(@PathVariable Long userId) {
        systemUserService.delete(userId);
    }

    @Operation(summary = "批量删除")
    @DeleteMapping("/deleteByIdList/{userIdList}")
    public void delete(@PathVariable List<Long> userIdList) {
        if (CollectionUtils.isEmpty(userIdList)) {
            return;
        }
        systemUserService.deleteByIdList(userIdList);
    }

}
