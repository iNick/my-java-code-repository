package xin.nick.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.domain.dto.RoleSetAuthorityListDTO;
import xin.nick.domain.vo.AuthorityVO;
import xin.nick.domain.vo.RoleSetAuthorityListVO;
import xin.nick.service.IRoleAuthorityService;

import java.util.List;

/**
 * <p>
 * 角色权限关联 前端控制器
 * </p>
 *
 * @author Nick
 * @since 2022-08-10
 */
@Tag(name = "角色权限管理")
@RestController
@RequestMapping("/api/role-authority")
public class RoleAuthorityController {

    @Autowired
    private IRoleAuthorityService roleAuthorityService;

    /**
     * 根据角色id获取权限列表
     * @param roleId
     * @return
     */
    @Operation(summary = "根据角色id获取权限列表")
    @GetMapping("/getAuthorityListByRoleId")
    public List<AuthorityVO> getAuthorityListByRoleId(@PathVariable Long roleId) {

        List<AuthorityVO> list = roleAuthorityService.getAuthorityListByRoleId(roleId);

        return list;

    }

    @Operation(summary = "设置角色关联的权限列表")
    @PostMapping("/setRoleAuthorityList")
    public RoleSetAuthorityListVO setRoleAuthorityList(@RequestBody RoleSetAuthorityListDTO roleSetAuthorityListDTO) {

        RoleSetAuthorityListVO roleSetAuthorityListVO = roleAuthorityService.setRoleAuthorityList(roleSetAuthorityListDTO);

        return roleSetAuthorityListVO;
    }

}
