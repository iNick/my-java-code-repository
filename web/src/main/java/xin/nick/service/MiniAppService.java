package xin.nick.service;

/**
 * @author Nick
 * @since 2022/7/5/005
 */
public interface MiniAppService {

    /**
     * 发送模板消息
     * @param message
     */
    void sendTemplateMessage(String message);

}
