package xin.nick.service;

import xin.nick.entity.WxUserInfoVO;

/**
 * 登录业务处理
 * @author Nick
 * @since 2022/6/21/021
 */
public interface LoginService {

    /**
     * 微信端登录
     * @param code
     * @return
     */
    WxUserInfoVO wxUserLogin(String code);

}
