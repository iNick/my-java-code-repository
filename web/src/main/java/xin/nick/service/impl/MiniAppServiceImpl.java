package xin.nick.service.impl;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.WxMaSubscribeService;
import cn.binarywang.wx.miniapp.bean.WxMaTemplateData;
import me.chanjar.weixin.common.bean.subscribemsg.TemplateInfo;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xin.nick.service.MiniAppService;

import java.util.List;

/**
 * @author Nick
 * @since 2022/7/5/005
 */
@Service
public class MiniAppServiceImpl implements MiniAppService {

    @Autowired
    WxMaService wxMaService;

    @Override
    public void sendTemplateMessage(String message) {


        WxMaTemplateData wxMaTemplateData = new WxMaTemplateData();

        WxMaSubscribeService wxMaSubscribeService = wxMaService.getSubscribeService();
        try {
            List<TemplateInfo> templateList = wxMaSubscribeService.getTemplateList();
        } catch (WxErrorException e) {
            throw new RuntimeException(e);
        }
    }
}
