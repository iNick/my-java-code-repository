package xin.nick.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.service.MiniAppService;

/**
 * @author Nick
 * @since 2022/7/5/005
 */
@RestController()
@RequestMapping("/wx")
public class MiniAppController {

    @Autowired
    private MiniAppService miniAppService;

    @PostMapping("/send/template/message")
    public void sendTemplateMessage(String message) {
        miniAppService.sendTemplateMessage(message);
    }

}
