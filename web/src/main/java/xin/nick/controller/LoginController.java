package xin.nick.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xin.nick.entity.WxUserInfoVO;
import xin.nick.service.LoginService;

/**
 * @author Nick
 * @since 2022/6/20/020
 */
@RestController()
@RequestMapping("/wx")
public class LoginController {

    @Autowired
    LoginService loginService;

    @PostMapping("/mini/login")
    public WxUserInfoVO wxMiniLogin(String code) {
        WxUserInfoVO wxUserInfoVO = loginService.wxUserLogin(code);
        return wxUserInfoVO;
    }

}
