package xin.nick.entity;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author Nick
 * @since 2022/6/20/020
 */
@Data
public class WxUserInfoVO {

    @Schema(description = "用户id")
    private Long userId;

    @Schema(description = "用户名")
    private String username;


}
