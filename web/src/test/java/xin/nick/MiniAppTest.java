package xin.nick;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.WxMaSubscribeService;
import cn.binarywang.wx.miniapp.bean.WxMaSubscribeMessage;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.bean.subscribemsg.CategoryData;
import me.chanjar.weixin.common.bean.subscribemsg.TemplateInfo;
import me.chanjar.weixin.common.error.WxErrorException;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * @author Nick
 * @since 2022/7/5/005
 */
@Slf4j
@SpringBootTest
public class MiniAppTest {

    @Autowired
    WxMaService wxMaService;

    /**
     * 获取模板类目列表
     */
    @Test
    void getCategoryList() {
        WxMaSubscribeService wxMaSubscribeService = wxMaService.getSubscribeService();
        try {
            List<CategoryData> categoryList = wxMaSubscribeService.getCategory();
            System.out.println(categoryList);
            Assertions.assertNull(categoryList);
        } catch (WxErrorException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 获取模板列表
     */
    @Test
    void getTemplateMessageList() {
        WxMaSubscribeService wxMaSubscribeService = wxMaService.getSubscribeService();
        try {
            List<TemplateInfo> templateList = wxMaSubscribeService.getTemplateList();
            System.out.println(templateList);
        } catch (WxErrorException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void test() {
        log.info("三十岁");
    }



    public static void main(String[] args) {
        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    }

    /**
     * 发送模板消息
     */
    @Test
    void sendTemplateMessage() {
        WxMaSubscribeService wxMaSubscribeService = wxMaService.getSubscribeService();
        try {


            WxMaSubscribeMessage wxMaSubscribeMessage = new WxMaSubscribeMessage();
            wxMaSubscribeMessage.setToUser("openId");
            wxMaSubscribeMessage.setTemplateId("模板id");
            wxMaSubscribeMessage.addData(new WxMaSubscribeMessage.MsgData("thing1", String.valueOf(System.currentTimeMillis())));
            wxMaSubscribeMessage.addData(new WxMaSubscribeMessage.MsgData("thing2", "张三"));
            wxMaSubscribeMessage.addData(new WxMaSubscribeMessage.MsgData("time3", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
            wxMaSubscribeMessage.addData(new WxMaSubscribeMessage.MsgData("thing4", "效果很好"));
            wxMaSubscribeService.sendSubscribeMsg(wxMaSubscribeMessage);

        } catch (WxErrorException e) {
            throw new RuntimeException(e);
        }
    }
}
